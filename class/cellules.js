class Cellule{
    constructor(value, visible = false, gotFlag = false){
        this.value = value;
        this.visible = visible;
        this.gotFlag = gotFlag;
    }
}


class Mine extends Cellule {
    constructor(visible, gotFlag){
        super('M', visible, gotFlag);
    }
}
exports.Mine = Mine;


class Nombre extends Cellule{
    constructor(value, visible, gotFlag){
        super(value, visible, gotFlag);
    }
}
exports.Nombre = Nombre;


