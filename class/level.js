const levelName = {
    LOW: 'low',
    MEDIUM: 'medium',
    HARD: 'hard',
    SUPERHARD: 'superhard'
};
exports.levelName = levelName;


class Level{
    constructor(level){
        switch (level) {
            case levelName.LOW:
                this.nbMine = 2;
                this.nbNumber = 23;
                this.nbCellule = 25;
                break;
            case levelName.MEDIUM:
                this.nbMine = 6;
                this.nbNumber = 58;
                this.nbCellule = 64;
                break;
            case levelName.HARD:
                this.nbMine = 12;
                this.nbNumber = 88;
                this.nbCellule = 100;
                break;
            case levelName.SUPERHARD:
                this.nbMine = 24;
                this.nbNumber = 76;
                this.nbCellule = 100;
                break;
        }
    }
}
exports.Level = Level;
