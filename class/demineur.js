const {Nombre, Mine} = require('./cellules.js');

class Demineur{
    #nbMine;
    #flagNumber;

    constructor(level, grille){
        this.defeat = false;
        this.win = false;
        this.#nbMine = level.nbMine;
        this.#flagNumber = 0;

        if(grille !== null && grille !== undefined){
            for(let y = 0; y<grille.length; y++){
                for(let x = 0; x<grille[y].length; x++){
                    if(typeof(grille[y][x]) === "string"){
                        grille[y][x] = new Mine();
                    }else if(typeof(grille[y][x]) === "number"){
                        grille[y][x] = new Nombre(grille[y][x]);
                    }
                }
            }
            this.grille = grille;
        }

        else{

            this.grille = this._createNewGrille(level);

            //Modif des nombres a cote des bombes
            for(let y = 0; y<this.grille.length; y++){
                for(let x = 0; x<this.grille[y].length; x++){
                    this._getNearCases(x, y, true);
                }
            }
        }
    }

    _createNewGrille(level){
        //création de la variable contenant les mines
        let mineTab = new Array(level.nbMine);
        for(let i = 0; i<mineTab.length; i++){
            mineTab[i] = new Mine();
        }
        //création de la variable contenant les nombres
        let numberTab = new Array(level.nbNumber);
        for(let i = 0; i<numberTab.length; i++){
            numberTab[i] = new Nombre(0);
        }

        //création d'une grille vide
        let newGrid = new Array(Math.sqrt(level.nbCellule));
        for (let i = 0; i < newGrid.length; i++) {
            newGrid[i] = new Array(Math.sqrt(level.nbCellule));
        }

        //mélange du tableau
        let tab = this._melangeTab(mineTab.concat(numberTab), level.nbCellule);
        let count = 0;
        //ajout des nombres et des mines dans la grille
        for(let y = 0; y<newGrid.length; y++){
            for(let x = 0; x<newGrid[y].length; x++){
                newGrid[y][x] = tab[count];
                count++;
            }
        }

        return newGrid;
    }

    _melangeTab(tab, nbCellule){
        function getRandomInt(max) {
            return Math.floor(Math.random() * Math.floor(max));
        }

        let a;
        let b;
        let newCellule;

        //melange une centaine de fois
        for(let i=0;i<100;i++){
            a = getRandomInt(nbCellule-1);
            b = getRandomInt(nbCellule-1);
            newCellule = tab[a];
            tab[a] = tab[b];
            tab[b] = newCellule;
        }

        return tab;
    }

    //Si createNewTab = true, initialise le tableau avec les bons nombres
    //Sinon vérifie et dévoile tout les 0 et cases à proximité
    _getNearCases(x, y, createNewTab){
        let xMax = 0;
        let yMax = 0;

        if(x >= this.grille[y].length-1)
            xMax = this.grille[y].length-1;
        else
            xMax = x +1;

        if(y >= this.grille.length-1)
            yMax = this.grille.length-1;
        else
            yMax = y +1;

        for(let line = y-1; line<=yMax; line++){
            for(let column = x-1; column<=xMax; column++){

                if(column < 0)
                    column = 0;

                if(line < 0)
                    line = 0;

                if(createNewTab){
                    if(this.grille[y][x].value !== 'M'){
                        if(this.grille[line][column].value === 'M'){
                            this.grille[y][x].value++;
                        }
                    }
                }else{
                    if((this.grille[line][column].value === 0) && (this.grille[line][column].visible === false)){
                        this.grille[line][column].visible = true;
                        this._getNearCases(column, line, false);
                    }else{
                        this.grille[line][column].visible = true;
                    }
                }
            }
        }
    }

    _getIfUserWin(){
        let win = true;
        for(let y = 0; y<this.grille.length; y++){
            for(let x = 0; x<this.grille[y].length; x++){
                if(this.grille[y][x].constructor.name === "Nombre"){
                    if(this.grille[y][x].visible === false){
                        win = false;
                    }
                }
            }
        }
        this.win = win;
    }

    display(){

        console.clear();

        console.log('nombre de flag = ' + this.#flagNumber);
        console.log('nombre de bombes = '+ this.#nbMine);

        let yToPrint = "  ";
        for(let y = 0; y<this.grille[0].length; y++){
            yToPrint = yToPrint + "  " + y.toString() + " ";
        }

        console.log(yToPrint);

        for(let y = 0; y<this.grille.length; y++){
            let tabToPrint = y.toString() + " | ";
            for(let x = 0; x<this.grille[y].length; x++){
                if(this.grille[y][x].visible === true){
                    tabToPrint = tabToPrint + this.grille[y][x].value + " | ";
                }else if(this.grille[y][x].gotFlag === true){
                    tabToPrint += "D | ";
                }
                else{
                    tabToPrint += "■ | ";
                }
            }
            console.log(tabToPrint);
        }
    }

    flag(x, y){
        if(x !== null && x !== undefined && y !== null && y !== undefined) {
            if (x < this.grille[0].length && y < this.grille.length) {
                if (this.grille[y][x].gotFlag) {
                    this.#flagNumber--;
                    this.grille[y][x].gotFlag = false;
                } else if (this.grille[y][x].visible === false) {
                    this.#flagNumber++;
                    this.grille[y][x].gotFlag = true;
                }
            }
        }
    }

    click(x, y){
        if(x !== null && x !== undefined && y !== null && y !== undefined){
            if(x < this.grille[0].length && y < this.grille.length){
                if(this.grille[y][x].value === 'M'){
                    this.grille[y][x].visible = true;
                    this.defeat = true;
                }else if(this.grille[y][x].value === 0){
                    this._getNearCases(x, y, false);
                    this._getIfUserWin();
                }else{
                    this.grille[y][x].visible = true;
                    this._getIfUserWin();
                }
            }
        }
    }
}
module.exports = Demineur;
