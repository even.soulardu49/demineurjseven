/***************************************MAIN***********************************************/

const scanf = require('scanf');
const {Level, levelName} = require('./class/level.js');
const Demineur = require('./class/demineur.js');

//grille prédéfinie d'exemple
/*const grid = [
    [0, 1, 1, 1, 0, 0],
    [0, 2, 'M', 2, 0, 0],
    [0,2, 'M', 2, 0, 0],
    [0, 1, 1, 1, 0, 0],
    [0, 0, 0, 0, 0, 0],
];*/


//prémice du jeu, qui va demander le niveau de jeu souhaité. Ne sert à rien si une grille est déjà prédéfinie.
let newLevel;
let gameStartNotFinish = false;

while(gameStartNotFinish !== true){
    console.log('***********Bienvenue dans le jeu du demineur************');
    console.log('A quel niveau de difficulté voulez vous jouez ? ');
    console.log('1- LOW   2- MEDIUM    3- HARD    4- SUPERHARD');
    let userChoice = scanf('%d');
    switch (userChoice) {
        case 1:
            newLevel =  new Level(levelName.LOW);
            gameStartNotFinish = true;
            break;
        case 2:
            newLevel =  new Level(levelName.MEDIUM);
            gameStartNotFinish = true;
            break;
        case 3:
            newLevel =  new Level(levelName.HARD);
            gameStartNotFinish = true;
            break;
        case 4:
            newLevel =  new Level(levelName.SUPERHARD);
            gameStartNotFinish = true;
            break;
        default:
            newLevel = new Level(levelName.LOW);
            gameStartNotFinish = true;
            break;
    }
}

//le constructeur peut prendre un niveau de jeu et une grille par défaut
// le niveau de jeu est inutile si la grille par défaut a été renseigné
demineur = new Demineur(newLevel);

//Jeu
while(!demineur.defeat && !demineur.win){
    demineur.display();
    console.log('Que voulez vous faire ?');
    console.log('1- Decouvrir une case   2- Placer un drapeau');
    let userChoice = scanf('%d');
    if(userChoice === 2){
        console.log('Entrer le x : ');
        let x = scanf('%d');
        console.log('Entrer le y : ');
        let y = scanf('%d');
        demineur.flag(x,y);
    }else{
        console.log('Entrer le x : ');
        let x = scanf('%d');
        console.log('Entrer le y : ');
        let y = scanf('%d');
        demineur.click(x,y);
    }
}

//Gestion victoire ou défaite
if(demineur.defeat){
    demineur.display();
    console.log('****************');
    console.log('YOU ARE A LOOSER');
    console.log('****************');
}else if(demineur.win){
    demineur.display();
    console.log('******************');
    console.log('YOU ARE A CHAMPION');
    console.log('******************');
}